'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const hash = require('object-hash')
const Config = use('Config')
const Cache = use('Cache')

class CacheMiddleware {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response, params, view }, next) {
    // call next to advance the request 
    await this.handleUpStream({ request, response, params, view }) 
    await next()
    
  }

  async handleUpStream ({ request, response, params, view }) {
    const key = this.cacheKey({ request, response, params, view })
    params.cache = { key, minutes: Config.get('cache.minutes', 10) }  
    
    // force cache
    if (params.refresh) { 
      Cache.forget(key) 
      return
    }

    const content = await Cache.get(key)  
    if (content) {
        response.implicitEnd = false
        response.send(content).end()
    }
    
    return
  }

  cacheKey ({ request, response, params, view }) {
    const { hkey } = params
    if (hkey) return hkey

    const queries = Object.entries(request.except(['csrf_token', 'submit', 'k', 'e', 'refresh']))
      .sort(([, v1], [, v2]) => +v2 - +v1)
      .reduce((r, [k, v]) => ({ ...r, [k]: v }), {}) 
      
    // remove format eg: json
    const format = request.format()
    
    const url = !format ? request.url() : request.url().replace(`.${format}`, '')

    // get key
    return hash({ url, queries })
  }

}

module.exports = CacheMiddleware
