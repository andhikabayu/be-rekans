'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */ 
class AuthJwt {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response, auth }, next) {
    // call next to advance the request
   
    try{
      await auth.check()
      let user = await auth.getUser()
      let dqa_id = user.toJSON().dqa_id_hash
      let msisdn = user.toJSON().msisdn_hash 
      if((dqa_id !== request.header('dqa_id_hash') || msisdn !== request.header('msisdn_hash'))) this.sendErrorResponse(response)
      if((!request.header('name') && !request.header('secret'))) this.sendErrorResponse(response)
    }catch(e){
      console.log(e)
      this.sendErrorResponse(response)
    }
    await next()
  }

  async sendErrorResponse(response){
    let message = 'Unauthorized.'
    let status = '401' 
    return response.status(status).json({ 
      status : status,
      message: message,
      data: [],
    })
  }

}

module.exports = AuthJwt
