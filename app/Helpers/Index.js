'use strict' 

const RandomString = (length)=>{ 
  let result           = ''
  let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let charactersLength = characters.length
  for ( let i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const RandomInt = (length)=>{ 
   let result           = ''
   let characters       = '0123456789'
   let charactersLength = characters.length
   for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
   }
   return result
 }

const SendResponse = (status,message,data)=>{   
    return { 
      status : status,
      message: message,
      data : data
    }
}

const isIMEIValid = (imei)=>{   
  if (!/^[0-9]{15}$/.test(imei)) {return false;}
    
  var sum = 0, factor = 2, checkDigit, multipliedDigit;
  
  for (var i = 13, li = 0; i >= li; i--) {
    multipliedDigit = parseInt(imei.charAt(i), 10) * factor;
    sum += (multipliedDigit >= 10 ? ((multipliedDigit % 10) + 1) : multipliedDigit);
    (factor === 1 ? factor++ : factor--);
  }
  checkDigit = ((10 - (sum % 10)) % 10);
  
  return !(checkDigit !== parseInt(imei.charAt(14), 10))
}

module.exports = {
   SendResponse, 
   RandomString, 
   isIMEIValid, 
   RandomInt
}