"use strict";

const DB = use("Database");
const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const { validate } = use("Validator");
const Hash = use("Hash");
const { isEmpty } = require("lodash");
const moment = use("moment");

const EventModel = use("App/Models/Event");

class EventController {
  async getAllEvent({ request, response }) {
    let userId = request.input("userId");
    try {
      let event = await DB.raw(
        `SELECT * FROM events WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      if (event.rows) {
        return SendResponse(200, success, event.rows, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async getTags({ request, response }) {
    try {
      let events = await EventModel.query()
        .select("tags")
        .where({ channel: request.input("channel") })
        .whereNotNull("tags")
        .fetch();
      if (events) {
        return SendResponse(200, success, events, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async store({ request, response }) {
    const rules = {
      event_name: "required",
      start_date: "required",
      end_date: "required",
      start_time: "required",
      end_time: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();
      let event = new EventModel();

      try {
        // store to db
        event.assignee = req.assignee;
        event.event_name = req.event_name;
        event.start_date = moment(req.start_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        event.end_date = moment(req.end_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        event.start_time = req.start_time;
        event.end_time = req.end_time;
        event.repeat = req.repeat;
        event.description = req.description;
        event.location = req.location;
        event.lat = req.lat;
        event.lng = req.lng;
        event.channel = req.channel;
        event.conv_name = req.conv_name;
        event.created_by = req.created_by;
        event.tags = req.tags;
        event.status = "Open";
        event.end_repeat =
          req.end_repeat === "-"
            ? null
            : moment(req.end_repeat, "DD-MM-YYYY").format("YYYY-MM-DD");
        if (await event.save()) {
          return SendResponse(
            200,
            "Create Event successfully.",
            event,
            response
          );
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async update({ request, response, params }) {
    let req = request.all();
    try {
      let event = await EventModel.find(params.id);
      event.event_name = req.event_name;
      event.start_date = moment(req.start_date, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      event.end_date = moment(req.end_date, "DD-MM-YYYY").format("YYYY-MM-DD");
      event.start_time = req.start_time;
      event.end_time = req.end_time;
      event.repeat = req.repeat;
      event.description = req.description;
      event.location = req.location;
      event.lat = req.lat;
      event.lng = req.lng;
      event.channel = req.channel;
      event.conv_name = req.conv_name;
      event.created_by = req.created_by;
      event.tags = req.tags;
      event.status = req.status;
      await event.save();
      return SendResponse(200, "Update Event successfully.", {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(200, "Bad Request", {}, response);
    }
  }
}

module.exports = EventController;
