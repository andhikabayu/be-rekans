"use strict";

const DB = use("Database");
const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const { validate } = use("Validator");
const Hash = use("Hash");
const { isEmpty } = require("lodash");
const Env = use("Env");
const AppUrl = Env.get("APP_URL");
const Drive = use("Drive");
const Helpers = use("Helpers");

const RequestModel = use("App/Models/Request");

class RequestController {
  async getAllRequest({ request, response }) {
    let userId = request.input("userId");
    try {
      let reqData = await DB.raw(
        `SELECT * FROM requests WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      if (reqData.rows) {
        return SendResponse(200, success, reqData.rows, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async getTags({ request, response }) {
    try {
      let reqData = await RequestModel.query()
        .select("tags")
        .whereNotNull("tags")
        .fetch();
      if (reqData) {
        return SendResponse(200, success, reqData, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async store({ request, response }) {
    const rules = {
      request_name: "required",
      assignee: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();

      let stringFileName = RandomString(11);

      try {
        // store to db
        let reqData = new RequestModel();

        reqData.request_name = req.request_name;
        reqData.assignee = req.assignee;
        reqData.complete_when = req.complete_when;
        reqData.attachment = req.attachment;
        reqData.description = req.description;
        reqData.channel = req.channel;
        reqData.location = req.location;
        reqData.lat = req.lat;
        reqData.lng = req.lng;
        reqData.conv_name = req.conv_name;
        reqData.created_by = req.created_by;
        reqData.approved_by_order = req.approved_by_order;
        reqData.status = "Open";
        reqData.attachment = "/attachment/" + stringFileName;

        if (await reqData.save()) {
          const file = request.file("attachment", {
            // types: ["image"],
            size: "2mb",
          });
          if (file) {
            // updated attachment name with extension
            reqData.attachment =
              "/attachment/" + stringFileName + "." + file.extname;
            await reqData.save();
            await file.move(Helpers.tmpPath("attachment"), {
              name: stringFileName + "." + file.extname,
              overwrite: true,
            });

            if (!file.moved()) {
              return SendResponse(400, badRequest, file.error(), response);
            }
          }

          return SendResponse(
            200,
            "Create Request successfully.",
            reqData,
            response
          );
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async download({ request, response, params }) {
    try {
      let getData = await RequestModel.find(params.id);
      if (getData) {
        // return getData.attachment
        const filePath = getData.attachment;
        const isExists = await Drive.exists(Helpers.tmpPath(filePath));
        // return isExists
        if (isExists) {
          return response.attachment(Helpers.tmpPath(filePath));
        }
        return SendResponse(
          400,
          badRequest,
          { message: "File does not exist" },
          response
        );
      }
      return SendResponse(
        400,
        badRequest,
        { message: "Data not found" },
        response
      );
    } catch (error) {
      return SendResponse(400, badRequest, error, response);
    }
  }

  async update({ request, response, params }) {
    let req = request.all();
    try {
      let reqData = await RequestModel.find(params.id);
      reqData.request_name = req.request_name;
      reqData.assignee = req.assignee;
      reqData.complete_when = req.complete_when;
      reqData.attachment = req.attachment;
      reqData.description = req.description;
      reqData.channel = req.channel;
      reqData.location = req.location;
      reqData.lat = req.lat;
      reqData.lng = req.lng;
      reqData.conv_name = req.conv_name;
      reqData.created_by = req.created_by;
      reqData.approved_by_order = req.approved_by_order;
      reqData.status = req.status;
      await reqData.save();
      return SendResponse(200, "Update Request successfully.", {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(200, "Bad Request", {}, response);
    }
  }
}

module.exports = RequestController;
