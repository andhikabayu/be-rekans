'use strict'

const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const SettingModel = use("App/Models/Setting");
const Event = use("App/Models/Event");
const Task = use("App/Models/Task");
const Request = use("App/Models/Request");
const { validate } = use("Validator");
const DB = use("Database");
const moment = use("moment");

class SettingController {
  async store({ request, response, auth }) {
    const rules = {
      // name: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();
      let setting = new SettingModel();
      if (req.user_id) setting = await SettingModel.findBy("user_id", req.user_id);

      try {
        // let dataUserLogged = await auth.getUser()
        
        // store to db
        setting.user_id = req.user_id;
        setting.dark_mode = req.dark_mode;
        setting.wallpaper = req.wallpaper;
        setting.enter_is_end = req.enter_is_end;
        setting.auto_save_gallery = req.auto_save_gallery;
        setting.notif_tone = req.notif_tone;
        setting.vibrate = req.vibrate;
        setting.led_notif = req.led_notif;
        setting.notif_tone_group_chat = req.notif_tone_group_chat;
        setting.vibrate_group_chat = req.vibrate_group_chat;
        setting.led_notif_group_chat = req.led_notif_group_chat;
        setting.task_notif_assigned_you = req.task_notif_assigned_you;
        setting.task_notif_pc = req.task_notif_pc;
        setting.on_mobile_data = req.on_mobile_data;
        setting.on_wifi = req.on_wifi;
        setting.on_roaming = req.on_roaming;
        setting.device_storage_setting = req.device_storage_setting;
        setting.size_setting = req.size_setting;
        setting.chat_storage_usage = req.chat_storage_usage;
        setting.last_seen = req.last_seen;
        setting.profile_photos_privacy = req.profile_photos_privacy;
        setting.about_privacy = req.about_privacy;
        setting.read_receipts = req.read_receipts;
        setting.invite_to_group = req.invite_to_group;
        setting.two_step_verification = req.two_step_verification;

        if (await setting.save()) {
          return SendResponse(200, "Store settings successfully.", setting, response);
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }
}

module.exports = SettingController
