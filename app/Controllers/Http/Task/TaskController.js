"use strict";

const DB = use("Database");
const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const { validate } = use("Validator");
const Hash = use("Hash");
const moment = use("moment");
const Env = use("Env");
const AppUrl = Env.get("APP_URL");
const Drive = use("Drive");
const Helpers = use("Helpers");

const TaskModel = use("App/Models/Task");

class TaskController {
  async getAllTask({ request, response }) {
    let userId = request.input("userId");
    try {
      let task = await DB.raw(
        `SELECT * FROM tasks WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      if (task.rows) {
        return SendResponse(200, success, task.rows, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async getTags({ request, response }) {
    try {
      let reqData = await TaskModel.query()
        .select("tags")
        .whereNotNull("tags")
        .fetch();
      if (reqData) {
        return SendResponse(200, success, reqData, response);
      }
      return SendResponse(400, badRequest, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async getChildTask({ request, response }) {
    try {
      let task = await TaskModel.query()
        .where({ channel: request.input("channel") })
        .orderBy("id", "asc")
        .fetch();

      return SendResponse(200, success, task, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, [], response);
    }
  }

  async store({ request, response }) {
    const rules = {
      task_name: "required",
      assignee: "required",
      start_date: "required",
      due_date: "required",
      due_time: "required",
      channel: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();
      let stringFileName = RandomString(11);

      let task = new TaskModel();

      task.task_name = req.task_name;
      task.is_subtask = req.is_subtask;
      task.assignee = req.assignee;
      task.start_date = moment(req.start_date, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      task.due_date = moment(req.due_date, "DD-MM-YYYY").format("YYYY-MM-DD");
      task.due_time = req.due_time === "0:0" ? "00:00" : req.due_time;
      task.repeat = req.repeat;
      task.complete_when = req.complete_when;
      task.description = req.description;
      task.tags = req.tags;
      task.created_by = req.created_by;
      task.attachment = req.attachment;
      task.is_subtask = req.is_subtask;
      task.channel = req.channel;
      task.location = req.location;
      task.lat = req.lat;
      task.lng = req.lng;
      task.conv_name = req.conv_name;
      task.end_repeat =
        req.end_repeat === "-"
          ? null
          : moment(req.end_repeat, "DD-MM-YYYY").format("YYYY-MM-DD");
      if (req.is_subtask) task.parent_task = req.parent_task;
      task.status = "Open";
      task.attachment = "/attachment/" + stringFileName;

      if (req.is_subtask) task.parent_task = req.parent_task;
      task.status = "Open";

      try {
        // store to db
        if (await task.save()) {
          const file = request.file("attachment", {
            // types: ["*"],
            size: "2mb",
          });
          if (file) {
            // update file name with extension
            task.attachment =
              "/attachment/" + stringFileName + "." + file.extname;
            await task.save();
            await file.move(Helpers.tmpPath("attachment"), {
              name: stringFileName + "." + file.extname,
              overwrite: true,
            });

            if (!file.moved()) {
              return SendResponse(400, badRequest, file.error(), response);
            }
          }

          return SendResponse(200, "Create Task successfully.", task, response);
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async update({ request, response, params }) {
    let req = request.all();
    try {
      let task = await TaskModel.find(params.id);
      task.task_name = req.task_name;
      task.is_subtask = req.is_subtask;
      task.assignee = req.assignee;
      task.start_date = moment(req.start_date, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      );
      // task.due_date = moment(req.due_date, "DD-MM-YYYY").format("YYYY-MM-DD");
      task.due_time = req.due_time === "0:0" ? "00:00" : req.due_time;
      task.repeat = req.repeat;
      task.complete_when = req.complete_when;
      task.description = req.description;
      task.tags = req.tags;
      task.created_by = req.created_by;
      task.attachment = req.attachment;
      task.is_subtask = req.is_subtask;
      task.channel = req.channel;
      task.location = req.location;
      task.lat = req.lat;
      task.lng = req.lng;
      task.conv_name = req.conv_name;
      task.progress = req.progress;
      if (req.complete_when === "Checked by all" && req.status === "Done")
        task.progress = parseInt(task.progress) + 1;
      if (req.is_subtask) task.parent_task = req.parent_task;
      task.status = req.status;
      if (parseInt(req.progress) === 100) task.status = "Complete";
      if (req.is_subtask) task.parent_task = req.parent_task;
      await task.save();
      return SendResponse(200, "Update Task successfully.", {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(200, "Bad Request", {}, response);
    }
  }

  async download({ request, response, params }) {
    try {
      let getData = await TaskModel.find(params.id);
      if (getData) {
        // return getData.attachment
        const filePath = getData.attachment;
        const isExists = await Drive.exists(Helpers.tmpPath(filePath));
        // return isExists
        if (isExists) {
          return response.attachment(Helpers.tmpPath(filePath));
        }
        return SendResponse(
          400,
          badRequest,
          { message: "File does not exist" },
          response
        );
      }
      return SendResponse(
        400,
        badRequest,
        { message: "Data not found" },
        response
      );
    } catch (error) {
      return SendResponse(400, badRequest, error, response);
    }
  }
}

module.exports = TaskController;
