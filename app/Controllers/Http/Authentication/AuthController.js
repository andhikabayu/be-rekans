"use strict";

const DB = use("Database");
const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const { validate } = use("Validator");
const Hash = use("Hash");
const { isEmpty } = require("lodash");
const nodemailer = require("nodemailer");
const Encryption = use("Encryption");

const UserModel = use("App/Models/User");

class AuthController {
  async register({ request, response, auth }) {
    const rules = {
      first_name: "required",
      last_name: "required",
      email: "required|email",
      password: "required",
    };
    let randomOtp = await RandomInt(4);
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();

      // validation password at least 6 character
      let pwdLength = req.password;
      if (pwdLength.length < 6) {
        return SendResponse(
          400,
          "Password must be at least 6 character.",
          {},
          response
        );
      }

      let checkEmail = await UserModel.findBy("email", req.email);
      // return checkEmail
      if (!isEmpty(checkEmail)) {
        return SendResponse(400, "Email address already exists.", {}, response);
      }

      let user = new UserModel();

      user.first_name = req.first_name;
      user.last_name = req.last_name;
      user.email = req.email;
      user.password = req.password;
      user.otp = randomOtp;

      try {
        // store to db
        if (await user.save()) {
          let validTkn = await auth
            .authenticator("jwt")
            .attempt(req.email, req.password);
          if (validTkn) user.token = validTkn.token;

          let arrOtp = user.otp.split("")
          let stringOtp = arrOtp.toString()

          let transporter = nodemailer.createTransport({
            service: "gmail",
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
              user: "fy.projectdev@gmail.com", // generated mailtrap user
              pass: "xhugwiywljxgudcs", // generated mailtrap password
            },
          });

          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"no reply mail" <no-reply@gmail.com>', // sender address
            to: user.email, // list of receivers
            subject: "OTP Verifikasi Akun", // Subject line
            //text: "Hello world?", // plain text body
            html: 
              '<body style="box-sizing: border-box;">'+
              '<div class="row">'+
                '<div style="text-align: left; float: left;width: 35%;padding: 50px;padding-left: 70px;height: 420px;">'+
                  '<p style="font-size: 20px;">Hi <span style="font-weight: bold;">'+user.first_name+' '+user.last_name+',</span></p>'+
                  '<br>'+
                  '<p style="font-size: 14px;">You almost ready to start enjoying with REKANS,<br>To verify your email address, use this security code:</p>'+
                  '<p style="font-size: 30px; line-height: 20px;"><span style="color: rgba(215,161,26,1);">'+stringOtp.replace(/,/g, " - ")+'</span><br><span style="font-size: 14px !important;">(This code will expired in 2 minutes)</span></p>'+
                  '<br>'+
                  '<br>'+
                  '<p style="font-size: 14px;">If you didn"t request this code, you can safely ignore this email.</p>'+
                  '<br>'+
                  '<br>'+
                  '<p style="font-size: 14px;">Warmly,<br><span style="font-family: Oleo Script; font-size: 14px; font-weight: bold;">Assistant Rekans</span></p>'+
                '</div>'+
                '<div style="float: left;width: 35%;padding: 50px;padding-left: 70px;height: 420px;">'+
                '<br>'+
                '<br>'+
                '<br>'+
                '<br>'+
                  '<img src="cid:unique@nodemailer.com" style="position: relative; top: 20%; width: 290px; right: 25%;">'+
                '</div>'+
              '</div>'+
              '<hr style="width: 55%; position: relative; right: 15%;">'+
              '<span style="width: 417px;color: rgba(215,161,26,1);position: absolute;top: 540px;left: 107px;font-family: Open Sans;font-weight: Bold;font-size: 14px;opacity: 1;text-align: left;">Get in touch</span>'+
              '<div style="position: absolute;bottom: 10%; left: 15%;"><img src="unique@gmail" width="40"></div>'+
              '<div style="position: absolute;bottom: 10.5%; left: 18.5%;"><img src="unique@instagram" width="30"></div>'+
              '<div style="width: 100%;height: 10px;background: rgba(0,25,40,1);opacity: 1;position: absolute;top: 600px;left: 0px;overflow: hidden;"></div>'+
            '</body>',
            attachments: [{
              filename: 'CreateAccount.PNG',
              path: 'asset/CreateAccount.PNG',
              cid: 'unique@nodemailer.com' //same cid value as in the html img src
            }]
          });
        }
        return SendResponse(
          200,
          "Register successfully.",
          { email: user.email, token: user.token },
          response
        );
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async login({ request, response, auth }) {
    const { email, password } = request.all();
    const rules = {
      email: "required|email",
      password: "required",
    };

    const validation = await validate(request.all(), rules);
    let checkVerifiedEmail = null;

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      try {
        checkVerifiedEmail = await UserModel.findBy("email", email);
        if (isEmpty(checkVerifiedEmail))
          return SendResponse(400, "Wrong email or password.", {}, response);
        // if (checkVerifiedEmail.is_verified === false) return SendResponse(400, 'Silahkan verifikasi OTP Anda terlebih dahulu.', [], response)

        if (checkVerifiedEmail.minutes_waiting_time != null) {
          let queryCheckAttempts =
            "select * from users" +
            " where (email = " +
            "'" +
            checkVerifiedEmail.email +
            "'" +
            " and failed_attempts_time < now() - interval '" +
            "" +
            checkVerifiedEmail.minutes_waiting_time +
            "" +
            " minutes' " +
            "and failed_attempts_counter >= 5)";
          let cekAttempts = await DB.raw(queryCheckAttempts);
          if (cekAttempts.rowCount == 0) {
            return SendResponse(
              400,
              "Too many failed login attempts, please try again in " +
                checkVerifiedEmail.minutes_waiting_time +
                " minutes.",
              {},
              response
            );
          }
        }

        let data = null;
        let token = null;
        let message = null;
        let status = 200;
        let validTkn = await auth.authenticator("jwt").attempt(email, password);
        if (validTkn) {
          checkVerifiedEmail.minutes_waiting_time = null;
          checkVerifiedEmail.failed_attempts_counter = null;
          checkVerifiedEmail.failed_attempts_time = null;
          await checkVerifiedEmail.save();
          data = checkVerifiedEmail;
          data.token = validTkn.token;
          message = "Login successfully.";
        }

        return response.json({
          status,
          message,
          data,
        });
      } catch (error) {
        // console.log(error)
        if (error.passwordField) {
          // check counter
          if (checkVerifiedEmail.failed_attempts_counter >= 5) {
            // update data menit tunggu
            checkVerifiedEmail.minutes_waiting_time =
              checkVerifiedEmail.failed_attempts_counter < 5
                ? 5
                : checkVerifiedEmail.minutes_waiting_time + 5;
            await checkVerifiedEmail.save();

            let queryGetUserAttempts =
              "select * from users" +
              " where (email = " +
              "'" +
              checkVerifiedEmail.email +
              "'" +
              " and failed_attempts_time < now() - interval '" +
              "" +
              checkVerifiedEmail.minutes_waiting_time +
              "" +
              " minutes' " +
              "and failed_attempts_counter >= 5)";
            let cekUserAttempts = await DB.raw(queryGetUserAttempts);

            if (
              checkVerifiedEmail.minutes_waiting_time >= 5 &&
              cekUserAttempts.rowCount == 0
            ) {
              return SendResponse(
                400,
                "Too many failed login attempts, please try again in " +
                  checkVerifiedEmail.minutes_waiting_time +
                  " minutes.",
                {},
                response
              );
            } else if (
              checkVerifiedEmail.minutes_waiting_time >= 5 &&
              cekUserAttempts.rowCount == 1
            ) {
              // update counter if attempt 5 times more
              checkVerifiedEmail.failed_attempts_counter =
                checkVerifiedEmail.failed_attempts_counter + 1;
              checkVerifiedEmail.failed_attempts_time = new Date();
              await checkVerifiedEmail.save();
              return SendResponse(
                400,
                "Too many failed login attempts, please try again in " +
                  checkVerifiedEmail.minutes_waiting_time +
                  " minutes.",
                {},
                response
              );
            }
          }

          // update counter if attempt 5 times
          checkVerifiedEmail.failed_attempts_counter =
            checkVerifiedEmail.failed_attempts_counter == null
              ? 1
              : checkVerifiedEmail.failed_attempts_counter + 1;
          checkVerifiedEmail.failed_attempts_time = new Date();
          await checkVerifiedEmail.save();

          if (checkVerifiedEmail.failed_attempts_counter == 5) {
            return SendResponse(
              400,
              "Too many failed login attempts, please try again in 5 minutes.",
              {},
              response
            );
          }
          return SendResponse(400, "Wrong email or password.", {}, response);
        }
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async verification({ params, response }) {
    try {
      const user = await UserModel.findBy("verification_token", params.token);
      if (user) {
        user.verification_token = null;
        user.is_active = 1;
        user.is_verified = 1;

        if (await user.save()) {
          return SendResponse(
            200,
            "Your email has been verified.",
            [],
            response
          );
        }
        return SendResponse(400, badRequest, {}, response);
      }
      return SendResponse(400, "email tidak ditemukan", {}, response);
    } catch (error) {
      // console.log(error)
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async verificationOtp({ request, response }) {
    const { email, otp } = request.all();
    const rules = {
      email: "required",
      otp: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      try {
        const user = await UserModel.query()
          .where("email", email)
          .where("otp", otp)
          .fetch();
        if (user.rows.length > 0) {
          let data = user.toJSON()[0];
          let userUpdate = await UserModel.find(data.id);
          userUpdate.otp = null;
          userUpdate.is_active = true;
          userUpdate.is_verified = true;
          await userUpdate.save();
          return SendResponse(
            200,
            "Your email has been verified.",
            {},
            response
          );
        } else {
          return SendResponse(400, "Wrong OTP.", {}, response);
        }
      } catch (error) {
        return SendResponse(400, badRequest, [], response);
      }
    }
  }

  async requestNewOtp({ request, response }) {
    const { email } = request.all();
    const rules = {
      email: "required|email",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      try {
        const user = await UserModel.query().where("email", email).fetch();
        if (user.rows.length > 0) {
          let data = user.toJSON()[0];
          let randomOtp = await RandomInt(4);

          let userUpdate = await UserModel.find(data.id);
          userUpdate.otp = randomOtp;
          userUpdate.is_active = false;
          userUpdate.is_verified = false;

          if (await userUpdate.save()) {
            let transporter = nodemailer.createTransport({
              service: "gmail",
              host: "smtp.gmail.com",
              port: 587,
              secure: false, // true for 465, false for other ports
              auth: {
                user: "fy.projectdev@gmail.com", // generated mailtrap user
                pass: "xhugwiywljxgudcs", // generated mailtrap password
              },
            });

            // send mail with defined transport object
            let info = await transporter.sendMail({
              from: '"no reply mail" <no-reply@gmail.com>', // sender address
              to: data.email, // list of receivers
              subject: "OTP Verifikasi Akun", // Subject line
              //text: "Hello world?", // plain text body
              html:
                '<center style="font-family: system-ui;">' +
                '<div style="margin-top: 20px; margin-bottom: 20px;">' +
                '<img src="#" alt="logo">' +
                "</div>" +
                "<br>" +
                '<div style="margin-bottom: 10px;">' +
                '<h2 style="font-weight: bold; font-size: 2rem;">Dear "' +
                data.first_name +
                " " +
                data.last_name +
                '"</h2>' +
                "</div>" +
                "<div>" +
                '<h6 style="font-size: 1rem;">Berikut adalah kode OTP Anda, silahkan masukan kode OTP dibawah ini kedalam aplikasi untuk <br>mengaktivasi akun Anda.</h6>' +
                "</div>" +
                "<div>" +
                '<h1 style="font-size: 3rem;">' +
                userUpdate.otp +
                "</h1>" +
                "</div>" +
                "</center>",
            });
            return SendResponse(200, "Resend OTP successfully.", {}, response);
          }
        } else {
          return SendResponse(400, "Email tidak ditemukan.", {}, response);
        }
      } catch (error) {
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async forgotPasswordOld({ request, response }) {
    const { email } = request.all();
    const user = await UserModel.query().where("email", email).fetch();

    const rules = {
      email: "required|email",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, badRequest, rspMsg, response);
    } else {
      try {
        if (user.rows.length > 0) {
          let data = user.toJSON()[0];
          const encrypted = Encryption.encrypt(data.email);
          data.url = "http://web.rekansapp.com/reset-password/" + encrypted;
          let transporter = nodemailer.createTransport({
            service: "gmail",
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
              user: "fy.projectdev@gmail.com", // generated mailtrap user
              pass: "xhugwiywljxgudcs", // generated mailtrap password
            },
          });

          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"no reply mail" <no-reply@gmail.com>', // sender address
            to: data.email, // list of receivers
            subject: "Reset Password", // Subject line
            //text: "Hello world?", // plain text body
            html:
              '<center style="font-family: system-ui;">' +
              '<div style="margin-top: 20px; margin-bottom: 20px;">' +
              '<img src="#" alt="logo">' +
              "</div>" +
              "<br>" +
              '<div style="margin-bottom: 10px;">' +
              '<h2 style="font-weight: bold; font-size: 2rem;">Dear "' +
              data.first_name +
              " " +
              data.last_name +
              '"</h2>' +
              "</div>" +
              "<div>" +
              '<h6 style="font-size: 1rem;">Kunjungi link berikut untuk mereset password akun Anda</h6>' +
              "</div>" +
              "<br>" +
              "<div>" +
              '<a href="' +
              data.url +
              '" >' +
              data.url +
              "</a>" +
              "</div>" +
              "</center>",
          });
          return SendResponse(
            200,
            "Your verification has been sent to your email",
            {},
            response
          );
        } else {
          return SendResponse(404, "Email not found.", {}, response);
        }
      } catch (error) {
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async forgotPassword({ request, response }) {
    const { email } = request.all();
    const user = await UserModel.query().where("email", email).fetch();

    const rules = {
      email: "required|email",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      try {
        if (user.rows.length > 0) {
          let randomOtp = await RandomInt(4);
          let data = user.toJSON()[0];
          let userUpdate = await UserModel.find(data.id);
          userUpdate.otp = randomOtp;
          await userUpdate.save();
          const encrypted = Encryption.encrypt(data.email);
          data.url = "http://web.rekansapp.com/reset-password/" + encrypted;
          let transporter = nodemailer.createTransport({
            service: "gmail",
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
              user: "fy.projectdev@gmail.com", // generated mailtrap user
              pass: "xhugwiywljxgudcs", // generated mailtrap password
            },
          });

          let arrOtp = userUpdate.otp.split("")
          let stringOtp = arrOtp.toString()

          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"no reply mail" <no-reply@gmail.com>', // sender address
            to: data.email, // list of receivers
            subject: "Verification Code", // Subject line
            //text: "Hello world?", // plain text body
            html: 
              '<body style="box-sizing: border-box;">'+
              '<div class="row">'+
                '<div style="text-align: left; float: left;width: 35%;padding: 50px;padding-left: 70px;height: 420px;">'+
                  '<p style="font-size: 20px;">Hi <span style="font-weight: bold;">'+data.first_name+' '+data.last_name+',</span></p>'+
                  '<br>'+
                  '<p style="font-size: 14px;">You are receiving this email as you have requested to reset your REKANS account password,<br>To verify your request use this security code:</p>'+
                  '<p style="font-size: 30px; line-height: 20px;"><span style="color: rgba(215,161,26,1);">'+stringOtp.replace(/,/g, " - ")+'</span><br><span style="font-size: 14px !important;">(This code will expired in 2 minutes)</span></p>'+
                  '<br>'+
                  '<br>'+
                  '<p style="font-size: 14px;">If you didn"t request this code, you can safely ignore this email.</p>'+
                  '<br>'+
                  '<br>'+
                  '<p style="font-size: 14px;">Warmly,<br><span style="font-family: Oleo Script; font-size: 14px; font-weight: bold;">Assistant Rekans</span></p>'+
                '</div>'+
                '<div style="float: left;width: 35%;padding: 50px;padding-left: 70px;height: 420px;">'+
                '<br>'+
                '<br>'+
                '<br>'+
                '<br>'+
                  '<img src="cid:unique@nodemailer.com" style="position: relative; top: 20%; width: 290px; right: 25%;">'+
                '</div>'+
              '</div>'+
              '<hr style="width: 55%; position: relative; right: 15%; top: 25px !important;">'+
              '<span style="width: 417px;color: rgba(215,161,26,1);position: absolute;top: 540px;left: 107px;font-family: Open Sans;font-weight: Bold;font-size: 14px;opacity: 1;text-align: left;">Get in touch</span>'+
              '<div style="position: absolute;bottom: 10%; left: 15%;"><img src="unique@gmail" width="40"></div>'+
              '<div style="position: absolute;bottom: 10.5%; left: 18.5%;"><img src="unique@instagram" width="30"></div>'+
              '<div style="width: 100%;height: 10px;background: rgba(0,25,40,1);opacity: 1;position: absolute;top: 600px;left: 0px;overflow: hidden;"></div>'+
            '</body>',
            attachments: [{
              filename: 'ResetAccount.PNG',
              path: 'asset/ResetAccount.PNG',
              cid: 'unique@nodemailer.com' //same cid value as in the html img src
            }]
          });
          return SendResponse(
            200,
            "Your verification has been sent to your email",
            {},
            response
          );
        } else {
          return SendResponse(404, "Email not found.", {}, response);
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async resetPassword({ request, response }) {
    const { email, password } = request.all();
    // const decrypted = Encryption.decrypt(email);
    const user = await UserModel.query().where("email", email).fetch();
    const rules = {
      email: "required",
      password: "required",
    };
    const validation = await validate(request.all(), rules);

    // validation password at least 6 character
    let pwdLength = password;
    if (pwdLength.length < 6) {
      return SendResponse(
        400,
        "Password must be at least 6 character.",
        {},
        response
      );
    }

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, badRequest, {}, response);
    } else {
      try {
        if (user.rows.length > 0) {
          let data = user.toJSON()[0];
          let userUpdate = await UserModel.find(data.id);
          userUpdate.password = password;
          await userUpdate.save();

          return SendResponse(
            200,
            "Change password successfully, please login.",
            {},
            response
          );
        } else {
          return response.json([
            {
              status: "error",
              message: "Url reset password expired",
            },
          ]);
        }
      } catch (error) {
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }
}

module.exports = AuthController;
