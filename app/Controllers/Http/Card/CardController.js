"use strict";

const { SendResponse, RandomString, RandomInt } = use("App/Helpers/Index.js");
const success = "Success",
  badRequest = "Bad Request";
const CardModel = use("App/Models/Card");
const Event = use("App/Models/Event");
const Task = use("App/Models/Task");
const Request = use("App/Models/Request");
const { validate } = use("Validator");
const DB = use("Database");
const moment = use("moment");

class CardController {
  async store({ request, response }) {
    const rules = {
      name: "required",
    };
    const validation = await validate(request.all(), rules);

    // check if validator fails
    if (validation.fails()) {
      const rspMsg = validation.messages()[0].message;
      return SendResponse(400, rspMsg, {}, response);
    } else {
      let req = request.all();
      let card = new CardModel();
      if (req.id) card = await CardModel.find(req.id);

      try {
        // store to db
        card.name = req.name;
        card.data_source = req.data_source;
        card.group_by = req.group_by;
        card.progress = req.progress;
        card.tags = req.tags;
        card.due_date = req.due_date;
        card.group_name = req.group_name;
        card.view = req.view;
        card.created_by = req.created_by;

        if (await card.save()) {
          return SendResponse(200, "Create Card successfully.", card, response);
        }
      } catch (error) {
        console.log(error);
        return SendResponse(400, badRequest, {}, response);
      }
    }
  }

  async getAll({ request, response }) {
    let req = request.all();
    let userId = req.created_by;
    let id = req.id;
    try {
      let task = await DB.raw(
        `SELECT * FROM tasks WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      let request = await DB.raw(
        `SELECT * FROM requests WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      let event = await DB.raw(
        `SELECT * FROM events WHERE string_to_array(assignee, ',') && array['${userId}'] order by id desc`
      );

      let card = await CardModel.query()
        .where(function (fn) {
          if (id)
            fn.where({
              id: id,
            });
          else
            fn.where({
              created_by: userId,
            });
        })
        .orderBy("id", "desc")
        .fetch();

      card = card.toJSON();

      let data = [];
      card.map((item) => {
        let obj = item;
        let tagsSplit = [];
        if (item.tags !== null) {
          tagsSplit = item.tags.split(",");
        }

        let status = ["Open", "On Progress", "Complete", "Done"];
        let where = "t.status === st";

        if (item.group_by === "Group Name") {
          if (item.data_source === "Task") {
            where = "t.conv_name && t.conv_name.indexOf(st) > -1";
            status = [];
            task.rows.map((t) => {
              if (t.conv_name === null) return;
              let convSplit = t.conv_name.split(",");
              convSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }

          if (item.data_source === "Event") {
            where = "t.conv_name && t.conv_name.indexOf(st) > -1";
            status = [];
            event.rows.map((t) => {
              if (t.conv_name === null) return;
              let convSplit = t.conv_name.split(",");
              convSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }

          if (item.data_source === "Request") {
            where = "t.conv_name && t.conv_name.indexOf(st) > -1";
            status = [];
            request.rows.map((t) => {
              if (t.conv_name === null) return;
              let convSplit = t.conv_name.split(",");
              convSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }
        }

        if (item.group_by === "Tags") {
          if (item.data_source === "Task") {
            where = "t.tags && t.tags.indexOf(st) > -1";
            status = [];
            task.rows.map((t) => {
              if (t.tags === null) return;
              let tagsSplit = t.tags.split(",");
              tagsSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }

          if (item.data_source === "Event") {
            where = "t.tags && t.tags.indexOf(st) > -1";
            status = [];
            event.rows.map((t) => {
              if (t.tags === null) return;
              let tagsSplit = t.tags.split(",");
              tagsSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }

          if (item.data_source === "Request") {
            where = "t.tags && t.tags.indexOf(st) > -1";
            status = [];
            request.rows.map((t) => {
              if (t.tags === null) return;
              let tagsSplit = t.tags.split(",");
              tagsSplit.map((sp) => {
                if (status.indexOf(sp) === -1) status.push(sp);
              });
            });
          }
        }

        if (item.progress !== "All")
          where = `t.status === "${item.progress}" && st === "${item.progress}"`;

        if (item.group_name && item.group_name !== "All")
          status = [item.group_name];

        if (item.due_date === "Overdue")
          where += ` && moment(t.due_date, "YYYY-MM-DD").toDate().getTime()< "${moment()
            .toDate()
            .getTime()}"`;

        if (item.due_date === "Today")
          where += ` && moment(t.due_date,"YYYY-MM-DD").format("YYYY-MM-DD") === "${moment().format(
            "YYYY-MM-DD"
          )}"`;

        if (tagsSplit.length > 0) where += ` && tagsSplit.indexOf(t.tags) > -1`;

        let tmpList = [],
          items = [];
        status.map((st) => {
          let tmpObj = {};
          tmpObj.name = st;
          let count = 0;

          if (item.data_source === "Task") {
            task.rows.map((t) => {
              if (eval(where)) {
                count++;
                t.name = t.task_name;
                t.data_source = item.data_source;
                items.push(t);
              }
            });
          }
          if (item.data_source === "Request") {
            request.rows.map((t) => {
              if (eval(where)) {
                count++;
                t.name = t.request_name;
                t.data_source = item.data_source;
                items.push(t);
              }
            });
          }
          if (item.data_source === "Event") {
            event.rows.map((t) => {
              if (eval(where)) {
                count++;
                t.name = t.event_name;
                t.data_source = item.data_source;
                items.push(t);
              }
            });
          }
          tmpObj.count = count;
          tmpList.push(tmpObj);
        });
        item.list = [];
        if (item.view === "Summary") obj.list = tmpList;
        item.items = [];
        if (item.view === "List") obj.items = items;
        data.push(obj);
      });

      if (data) {
        return SendResponse(200, success, data, response);
      }
      return SendResponse(400, badRequest, [], response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, [], response);
    }
  }

  async delete({ request, response }) {
    try {
      let card = await CardModel.find(request.input("id"));
      await card.delete();
      return SendResponse(200, success, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async getGroupName({ request, response }) {
    let source = request.input("source");
    let userId = request.input("userId");
    if (source === "Select Data")
      return SendResponse(200, success, [], response);
    try {
      source = source + "s";
      let reqData = await DB.raw(
        `SELECT DISTINCT conv_name FROM ${source} WHERE string_to_array(assignee, ',') && array['${userId}'] `
      );

      if (reqData.rows) {
        return SendResponse(200, success, reqData.rows, response);
      }
      return SendResponse(400, badRequest, [], response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, [], response);
    }
  }

  async getTags({ request, response }) {
    let source = request.input("source");
    let userId = request.input("userId");
    console.log(source);
    if (source === "Select Data")
      return SendResponse(200, success, [], response);
    try {
      source = source + "s";
      let reqData = await DB.raw(
        `SELECT DISTINCT tags FROM ${source} WHERE tags is not null and string_to_array(assignee, ',') && array['${userId}'] `
      );

      if (reqData.rows) {
        return SendResponse(200, success, reqData.rows, response);
      }
      return SendResponse(400, badRequest, [], response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, [], response);
    }
  }

  async deleteItem({ request, response }) {
    let id = request.input("id");
    let data_source = request.input("data_source");
    try {
      if (data_source === "Task") {
        let task = await Task.find(id);
        await task.delete();
      }
      if (data_source === "Event") {
        let event = await Event.find(id);
        await event.delete();
      }
      if (data_source === "Request") {
        let req = await Request.find(id);
        await req.delete();
      }

      return SendResponse(200, success, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }

  async duplicateItem({ request, response }) {
    let id = request.input("id");
    let data_source = request.input("data_source");
    try {
      if (data_source === "Task") {
        let task = await Task.find(id);
        let newTask = new Task();

        newTask.task_name = task.task_name;
        newTask.is_subtask = task.is_subtask;
        newTask.assignee = task.assignee;
        newTask.start_date = moment(task.start_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        newTask.due_date = moment(task.due_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        newTask.due_time = task.due_time;
        newTask.complete_when = task.complete_when;
        newTask.description = task.description;
        newTask.tags = task.tags;
        newTask.created_by = task.created_by;
        newTask.attachment = task.attachment;
        newTask.is_subtask = task.is_subtask;
        newTask.channel = task.channel;
        newTask.location = task.location;
        newTask.lat = task.lat;
        newTask.lng = task.lng;
        newTask.conv_name = task.conv_name;
        newTask.repeat = task.repeat;
        newTask.end_repeat = task.end_repeat;
        newTask.parent_task = task.parent_task;
        newTask.status = task.status;
        newTask.attachment = task.attachment;
        newTask.is_subtask = task.is_subtask;
        newTask.parent_task = task.parent_task;
        await newTask.save();
      }
      if (data_source === "Event") {
        let event = await Event.find(id);
        let newEvent = new Event();
        newEvent.assignee = event.assignee;
        newEvent.event_name = event.event_name;
        newEvent.start_date = moment(event.start_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        newEvent.end_date = moment(event.end_date, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        newEvent.start_time = event.start_time;
        newEvent.end_time = event.end_time;
        newEvent.repeat = event.repeat;
        newEvent.description = event.description;
        newEvent.location = event.location;
        newEvent.lat = event.lat;
        newEvent.lng = event.lng;
        newEvent.channel = event.channel;
        newEvent.conv_name = event.conv_name;
        newEvent.created_by = event.created_by;
        newEvent.tags = event.tags;
        newEvent.status = event.status;
        newEvent.end_repeat = event.end_repeat;
        await newEvent.save();
      }
      if (data_source === "Request") {
        let requestData = await Request.find(id);
        let newRequest = new Request();
        newRequest.request_name = requestData.request_name;
        newRequest.assignee = requestData.assignee;
        newRequest.complete_when = requestData.complete_when;
        newRequest.attachment = requestData.attachment;
        newRequest.description = requestData.description;
        newRequest.channel = requestData.channel;
        newRequest.location = requestData.location;
        newRequest.lat = requestData.lat;
        newRequest.lng = requestData.lng;
        newRequest.conv_name = requestData.conv_name;
        newRequest.created_by = requestData.created_by;
        newRequest.approved_by_order = requestData.approved_by_order;
        newRequest.status = requestData.status;
        newRequest.attachment = requestData.attachment;
        await newRequest.save();
      }

      return SendResponse(200, success, {}, response);
    } catch (error) {
      console.log(error);
      return SendResponse(400, badRequest, {}, response);
    }
  }
}

module.exports = CardController;
