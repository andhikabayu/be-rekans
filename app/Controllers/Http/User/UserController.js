'use strict'

const { update } = require("lodash")

const Database = use('Database')
const {SendResponse, RandomString, isIMEIValid, RandomInt} = use('App/Helpers/Index.js')
const success = "Success", badRequest = "Bad Request"
const { validate } = use('Validator')
var Env = use('Env')
var moment = require('moment');
const UserModel = use('App/Models/User')

class UserController {
    async getUser ({ request, response, auth }) {
        try{ 
            let user = await auth.getUser()
            if (user) {
                return SendResponse(200, success, user, response)   
            }
            return SendResponse(400, badRequest, {}, response)
        }catch(error){  
            return SendResponse(400, badRequest, {}, response)
        }
    }
}

module.exports = UserController
