"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.get("/", () => "{}");
Route.get("/api", () => "{}");
Route.get("/api/v1", () => "{}");

Route.group(() => {
  // user profile
  Route.get("user", "User/UserController.getUser");

  // event
  Route.get("event", "Event/EventController.getAllEvent");
  Route.get("event/tags", "Event/EventController.getTags");
  Route.post("event/store", "Event/EventController.store");
  Route.post("event/update/:id", "Event/EventController.update");

  // task
  Route.get("task/child", "Task/TaskController.getChildTask");
  Route.get("task", "Task/TaskController.getAllTask");
  Route.get("task/tags", "Task/TaskController.getTags");
  Route.post("task/store", "Task/TaskController.store");
  Route.post("task/update/:id", "Task/TaskController.update");
  Route.get("task/download-file/:id", "Task/TaskController.download");

  // request
  Route.get("request", "Request/RequestController.getAllRequest");
  Route.get("request/tags", "Request/RequestController.getTags");
  Route.post("request/store", "Request/RequestController.store");
  Route.get("request/download-file/:id", "Request/RequestController.download");
  Route.post("request/update/:id", "Request/RequestController.update");

  // card
  Route.get("card", "Card/CardController.getAll");
  Route.post("card/store", "Card/CardController.store");
  Route.delete("card", "Card/CardController.delete");
  Route.delete("card-item", "Card/CardController.deleteItem");
  Route.post("card-item-duplicate", "Card/CardController.duplicateItem");
  Route.get("card/group", "Card/CardController.getGroupName");
  Route.get("card/tags", "Card/CardController.getTags");

  // setting
  // Route.get("setting", "Settings/SettingController.getAll");
  Route.post("setting/store", "Settings/SettingController.store");
})
  // .middleware(['auth:jwt'])
  .prefix("api/v1");

Route.group(() => {
  // authentications
  Route.post("auth/register", "Authentication/AuthController.register");
  Route.post("auth/login", "Authentication/AuthController.login");
  Route.get(
    "auth/verification/:token",
    "Authentication/AuthController.verification"
  );
  Route.post(
    "auth/verification-otp",
    "Authentication/AuthController.verificationOtp"
  );
  Route.post(
    "auth/request-new-otp",
    "Authentication/AuthController.requestNewOtp"
  );
  Route.post(
    "auth/forgot-password",
    "Authentication/AuthController.forgotPassword"
  );
  Route.post(
    "auth/reset-password",
    "Authentication/AuthController.resetPassword"
  );
}).prefix("api/v1");
