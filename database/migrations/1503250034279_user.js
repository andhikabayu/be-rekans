'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('first_name', 255).notNullable()
      table.string('last_name', 255).notNullable()
      table.string('email', 255).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('otp', 4).nullable().defaultTo(null)
      table.boolean('is_verified').notNullable().defaultTo(0)
      table.boolean('is_active').notNullable().defaultTo(0)
      table.integer('minutes_waiting_time').nullable().defaultTo(null)
      table.integer('failed_attempts_counter').nullable().defaultTo(null)
      table.timestamp('failed_attempts_time').nullable().defaultTo(null)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
